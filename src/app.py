#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from healthcheck import HealthCheck
from flask_rq2 import RQ
from flask_rq2.job import FlaskJob
from flask import Flask, jsonify, request, app, send_from_directory, make_response, abort
import json, uuid, os, pypandoc
from pylatex import Document, Section, Command
from pylatex.base_classes import Options, Arguments
from pylatex.utils import NoEscape
from pylatex.section import Chapter
from pylatex.package import Package
from pylatex.basic import NewPage

redis_host = os.environ.get('REDIS_HOST')
redis_port = os.environ.get('REDIS_PORT')
redis_queue = os.environ.get('REDIS_QUEUE')

languages = {
  'it': 'italian',
  'en': 'english',
  'fr': 'french',
  'es': 'spanish',
  'ca': 'catalan',
  'de': 'german',
}

app = Flask(__name__)
app.config['RQ_REDIS_URL'] = 'redis://' + redis_host + ':' + redis_port + '/0'
app.config['RQ_QUEUES'] = [redis_queue]
rq = RQ(app)
rq.default_queue = redis_queue
health = HealthCheck(app, '/_meta/status')

@app.route('/v1/math-verifier/inline', methods = ['POST'])
def verify_math_formula_inline():
  formula = request.json['formula']
  return verify_math_formula(formula, '$')

@app.route('/v1/math-verifier/block', methods = ['POST'])
def verify_math_formula_block():
  formula = request.json['formula']
  return verify_math_formula(formula, '$$')

@app.route('/v1/convert', methods = ['POST'])
def convert():
  payload = request.json
  job_uuid = uuid.uuid4()
  conversion.queue(payload, job_id=str(job_uuid))
  return jsonify(job_id=job_uuid)

@app.route('/v1/jobs/<uuid:job_uuid>', methods = ['GET'])
def job_status(job_uuid):
  job = FlaskJob.fetch(str(job_uuid), rq.connection)
  if job.is_finished:
    result = {'status': 'finished', 'pdf_id': job.result}
  elif job.is_queued:
    result = {'status': 'queued'}
  elif job.is_started:
    result = {'status': 'started'}
  elif job.is_failed:
    result = {'status': 'failed'}
  app.logger.debug(result)
  return jsonify(result)

@app.route('/v1/pdf/<path:pdf_id>', methods = ['GET'])
def download(pdf_id):
  filepath = '/data/{}/'.format(pdf_id)
  for file in os.listdir(filepath):
    if file.endswith('.pdf'):
        filename = file
  app.logger.debug('{}{}'.format(filepath, filename))
  return send_from_directory(filepath, filename, as_attachment=True, attachment_filename=filename)

@rq.job
def conversion(payload):
  id = payload['_id']
  version = payload['_version']
  filepath = '/data/{}_{}'.format(id, version)
  return create_document(payload, filepath)

def create_document(payload, filepath):
  doc = CustomReport()

  doc.preamble.append(Command('setdefaultlanguage', arguments=Arguments(languages[payload['language']])))
  doc.preamble.append(Command('title', payload['title']))
  doc.preamble.append(Command('author', 'WikiToLearn contributors'))
  doc.preamble.append(Command('date', NoEscape(r'\today')))
  doc.append(NoEscape(r'\maketitle'))
  doc.append(NoEscape(r'\tableofcontents'))
  doc.append(NewPage())

  for chapter in payload['chapters']:
    with doc.create(Chapter(chapter['title'])):
        for page in chapter['pages']:
          with doc.create(Section(page['title'])):
            converted_text = pypandoc.convert_text(page['text'], to='latex', format='html')
            # FIXME: pandoc uses some custom commands which are not recognized
            converted_text = converted_text.replace(r'\tightlist', r'')
            converted_text = converted_text.replace(r'\toprule', r'')
            converted_text = converted_text.replace(r'\midrule', r'')
            converted_text = converted_text.replace(r'\bottomrule', r'')
            doc.append(NoEscape(converted_text))

  doc.append(NoEscape(r'\begin{appendix}'))
  doc.append(NoEscape(r'\listoffigures'))
  doc.append(NoEscape(r'\listoftables'))
  doc.append(NoEscape(r'\end{appendix}'))

  app.logger.debug(doc.dumps())
  os.makedirs(filepath, exist_ok=True)
  complete_filepath ='{}/{}'.format(filepath, payload['title'])
  doc.generate_pdf(filepath=complete_filepath, compiler='xelatex', clean=True, clean_tex=True, compiler_args=[])
  return filepath

def verify_math_formula(formula, decorator):
  os.makedirs('/data/math', exist_ok=True)
  id = uuid.uuid4()
  filepath = '/data/math/{}'.format(id)

  doc = CustomReport()
  full_formula = decorator + formula + decorator
  doc.append(NoEscape(full_formula))
  app.logger.debug(doc.dumps())

  try:
    doc.generate_pdf(filepath=filepath, compiler='xelatex', clean=True, clean_tex=True, compiler_args=[])
    os.remove('{}.pdf'.format(filepath))
    return jsonify(success=True)
  except Exception as e:
    return abort(make_response(jsonify(success=False, error=str(e)), 400))

class CustomReport(Document):
  def __init__(self):
    super().__init__(
      fontenc=None,
      inputenc=None,
      documentclass='report',
      document_options=['11pt', 'onecolumn', 'oneside', 'fleqn', 'a4paper'],
      geometry_options=['a4paper', 'top=3cm', 'bottom=3cm', 'left=3.5cm', 'right=3.5cm', 'heightrounded', 'bindingoffset=5mm']
    )
    self.preamble.append(Package('polyglossia'))
    self.preamble.append(Package('hyperref'))
    self.preamble.append(Package('longtable'))
    self.preamble.append(Package('amsmath'))
    self.preamble.append(Package('amsthm'))
    self.preamble.append(Package('amstext'))
    self.preamble.append(Package('amssymb'))
    self.preamble.append(Package('amsfonts'))
    self.preamble.append(Command('setlength', arguments=Arguments(NoEscape(r'\mathindent'), '0pt')))
    self.preamble.append(Package('listings'))
    self.preamble.append(Package('fancyvrb'))
    self.preamble.append(Package('booktabs'))
    self.preamble.append(Package('graphicx'))
    self.preamble.append(Package('grffile'))
    self.preamble.append(Package('xcolor', options=Options('usenames', 'table')))
    self.preamble.append(Package('ulem'))
    # It is used by Pandoc but breaks xelatex with polyglossia
    # self.preamble.append(Package('csquotes'))
    self.preamble.append(Package('fontspec'))
    self.preamble.append(Package('xunicode'))
    self.preamble.append(Package('xltxtra'))
    self.preamble.append(Package('anyfontsize'))
    self.preamble.append(Package('tabularx'))
    self.preamble.append(Package('float'))
    self.preamble.append(Package('cancel'))
    self.preamble.append(Package('tikz'))
    self.preamble.append(Package('enumitem'))
    self.preamble.append(Package('datetime', options=Options('nodayofweek')))
